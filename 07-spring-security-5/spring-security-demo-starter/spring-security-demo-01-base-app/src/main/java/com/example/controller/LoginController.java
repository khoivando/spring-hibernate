package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * @author KhoiDV
 * @date 3/5/2019
 */
@Controller
public class LoginController {

    @GetMapping("/login")
    public String login() {
        //return "plain-login";
        return "fancy-login";
    }
}
